# Angular 4.x build #

This image is automatically built by Docker Cloud and pushed to `trillrdev/angular-4.x-build:latest` which is a public image!

# How to use #

From project root folder:

	docker run --rm -v $PWD:/app trillrdev/angular-4.x-build:latest
	
This will build a production distribution in `dist`


